from flask import Flask
import os
import transporter as t
import jobs as j
import house as h

app = Flask(__name__)
cf_port = os.getenv("PORT")


@app.route('/house-details')
def house_details():
    url = r'https://www.olx.in/puducherry_g4059067/houses-villas-for-rent-houses-apartments_c1723?filter=bachelors_eq_yes%2Cfurnished_eq_yes%2Clisted_by_eq_owner%2Crooms_max_2%2Ctype_eq_houses'
    raw_html = t.simple_get(url)

    html = t.BeautifulSoup(raw_html, 'html.parser')
    # j.Jobs.get_jobs(html)
    details = h.House.get_details(html)
    return details


@app.route('/job-details')
def job_details():
    url = r'https://www.olx.in/puducherry_g4059067/data-entry-back-office_c1737?filter=job_type_eq_fulltime'
    raw_html = t.simple_get(url)

    html = t.BeautifulSoup(raw_html, 'html.parser')
    details = j.Jobs.get_jobs(html)
    return details


if __name__ == '__main__':
    if cf_port is None:
        app.run(host='localhost', port=5000, debug=True)
    else:
        app.run(host='0.0.0.0', port=int(cf_port), debug=True)